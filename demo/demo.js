var isLogin = true;
isLogin = false;

// var ss1 = 2 > 5;
var ss1 = 2 < 5;
var ss2 = 2 >= 1;
var ss3 = 2 == "2";
var ss4 = 2 === "2";
var ss6 = "a" == "a";
var ss5 = 2 != 2;
// != : so sánh khác ( khác biệt )
// &&

// var ss5 = 2 > 1 && 2 > 5;
var ss5 = true && false;
var ss6 = false && true;
// ss &&:(only) chỉ true khi tất cả điều kiện đều true ~ khó tính
var ss7 = false || true;
// ss ||: chỉ false khi tất cả điều kiện đều false ~ dễ tính

var ss8 = true && false && true && true;
var ss9 = false && false && false;
var ss10 = true || false || false || false;

console.log(`  🚀 __ file: demo.js __ line 25 __ ss10`, ss10);
