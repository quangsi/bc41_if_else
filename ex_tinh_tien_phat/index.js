function inKetQua() {
  var tienMuon = document.getElementById("txt-tien-muon").value * 1;
  var tienDaTra = document.getElementById("txt-tien-da-tra").value * 1;

  if (tienMuon > tienDaTra) {
    var tienPhat = (tienMuon - tienDaTra) * 0.015;
    document.getElementById(
      "result"
    ).innerText = `Số tiền phạt của bạn là ${tienPhat}`;
  } else {
    document.getElementById("result").innerText = "Bạn không bị phạt";
  }
}
